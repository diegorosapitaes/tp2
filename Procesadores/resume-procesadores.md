# Organización de una computadora

<div style="text-align: justify">

La **CPU (unidad central de procesamiento)**, es el cerebro de la computadora. Su función es ejecutar programas almacenados en la memoria principal buscando sus instrucciones y examinándolas para después ejecutarlas unas tras otras. La CPU se compone de varias partes, entre ellas se encuentran la CU (unidad de control), la ALU (unidad aritmética lógica), y la Memoria Central.

La CPU también contiene una memoria pequeña y de alta velocidad que sirve para almacenar resultados temporales y cierta información de control. Esta memoria se compone de varios registros. El registro más importante es el **PC (contador de programa)**, que apunta a la siguiente instrucción que debe buscarse para ejecutarse. Otro registro importante es el **IR (registro de instrucciones)** que contiene la instrucción que se está ejecutando. Casi todas las computadoras poseen algunos registros más con otros fines específicos.
### ALU: (Unidad Aritmética Lógica)
Es un circuito digital que calcula las operaciones aritméticas para el procesador y también hace operaciones lógicas, las operaciones aritméticas son tales como suma, resta, la multiplicación y las demás operaciones aritméticas, y las operaciones lógicas hacen lo que hacen nuestras compuertas lógicas.
### CU: (Unidad de Control)
La función de este elemento es buscar las instrucciones dentro de la memoria central o también llamada memoria principal, decodificarla y ejecutarlas, empleando la unidad de proceso. El CU se apoya de la memoria principal. Dentro de la memoria central se encuentra el programa, que éste primero se debe transferir a la memoria secundaria y luego a la memoria principal antes de que pueda ser ejecutado y los datos de entrada se deben proporcionar por alguna fuente de nuestros dispositivos de entrada como el teclado o el ratón.

Toda la información de una memoria principal se puede acceder y manipular mediante la unidad central de proceso CPU, cuyos resultados de esta manipulación se almacenan nuevamente en nuestra memoria central para finalmente tener los resultados de la memoria principal, y se pueda visualizar en un dispositivo de salida mediante datos de salida, y estos datos se guardan en nuestra memoria secundaria o externa para enviarse a otra computadora o a la red.

La CPU también contiene una memoria pequeña y de alta velocidad que sirve para almacenar resultados temporales y cierta información de control. Esta memoria se compone de varios registros. El registro más importante es el PC (contador de programa), que apunta a la siguiente instrucción que debe buscarse para ejecutarse. Otro registro importante es el IR (registro de instrucciones) que contiene la instrucción que se está ejecutando. Casi todas las computadoras poseen algunos registros más con otros fines específicos.
### Buses:
Son canales de datos que interconectan los componentes de nuestra computadora y algunos están diseñados para transferencias pequeñas y otras para transferencias mayores.
Existen tres tipos de buses:
- Bus Frontal: Bus fsb
- Bus de sistema: Existe en sistemas más antiguos y conectan al CPU con el procesador y la memoria RAM
- Buses E/S o I/O: Conectan la placa base de la computadora que es la mainboard con los dispositivos periféricos y por eso se denominan entrada/salida o imput/output.

### Conclusión
Las computadoras son muy importantes para todas las personas debido a que nos ayuda a mejorar nuestra calidad de vida, a hacer el trabajo más rápido y hasta con mejor presentación, además nos permite obtener cualquier información deseada en la red (Internet), comunicarnos con nuestros familiares que se encuentren en otro país por medio de chats o por mails, además nos proporcionan entretenimiento con juegos, ya sea que hayan sido bajados de la red o que se encuentren en discos o CDS (periféricos).
</div>

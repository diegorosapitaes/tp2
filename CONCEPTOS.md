# Parte 1: Git y Mark down:

## 1b. Definiciones y Conceptos:

### Software de control de versiones:
Git permite que a medida que vamos trabajando en un proyecto y queremos agregar nuevas funcionalidades, poder crear una rama (branch) para dejar intacta la principal (master) y sobre las ramas, hacer todo tipo de pruebas y modificaciones sin afectar la principal, lo cual resulta muy útil.

### Repositorios: Públicos o Privados.
Básicamente un repositorio (directorio de trabajo), es la carpeta principal donde se encuentran almacenados los archivos que componen el proyecto. Un repositorio público puede ser descargado como repositorio para hacer (en otro lugar), todos los cambios y seguir con el proyecto. Mientras que el privado solo es accesible si el propietario lo permite.

### Árboles de merke y su uso en Git:
Un árbol de merkle es una estructura dividida en capas que posee la finalidad de relacionar cada nodo con una raíz única asociada a los mismos. Git esta estructurado de tal manera que se va dividiendo en bloques, la estructura del árbol facilita la verificación de todos estos bloques de datos y además sirve de mecanismo para evitar la manipulación de la rama principal con lo que asegura el progreso del proyecto evitando todo tipo de riesgo de modificar algo indebidamente, teniendo la opción de regresar a una versión anterior en el progreso en cualquier momento.

### ¿Qué es un commit y cuando hacerlo?
A lo largo del desarrollo de un proyecto uno puede hacer en cualquier momento un “commit” del mismo, esto quiere decir que guardará el proyecto en el punto que se encuentra en ese momento, sin superponerse con etapas anteriores, teniendo la opción a futuro de ver cualquier commit que se ha hecho. Usualmente a cada commit que se hace, se le asigna una pequeña descripción de los cambios que se han hecho para identificar rápidamente la situación del proceso realizado hasta entonces.

### ¿Cómo volver a una versión anterior?
A medida que avanzamos en el proyecto y ya hemos los pasos iniciales de configuración de git como por ejemplo, decirle cual es nuestro email y nombre, los commit se van a ir añadiendo al proyecto que estemos realizando. Todo esos cambios lo vamos a poder ver desde la página de Gitlab o Github a la cual nos hallamos suscrito. Desde éstas herramientas (Gitlab o Github), podremos administrar nuestros proyectos, con todas sus modificaciones, commits, fechas de modificación de ramas, y todo lo necesario para llevar el control del proyecto.

### ¿Qué son los branches (ramas) y para qué se usan?
Los branches son esas ramas que mencionamos anteriormente en las cuales uno puede ir generando y trabajando sobre ellas para no modificar el árbol principal del proyecto (master), se utilizan para crear features, arreglar bugs, experimentar, etc.

# Markdown:
### 2.b) 1. ¿Qué es el lenguaje Markdown?
Markdown es una forma de agregar formato a textos web. Markdown fué creado con el proposito de crear un texto plano facil de escribir y facil de leer, y que pudiera convertirce de forma sensilla en XHTML. Permite crear paginas web sin tener experiencia en HTML.
Markdown es un sistema de procesado de textos que permite escribir rápido sin prestar demasiada atención a los formatos.

### 2. ¿Para qué se utiliza y cuales son las ventajas de este lenguaje?
Principalmente se utiliza para la escritura en la web ya que es más rápido y cómodo. Si uno se acostumbra a escribir con Markdown y conoce minimamente la sintaxis, se completa el texto más rápido.
En ocaciones uno solamente quiere escribir un texto determinado sin demasiados formatos, ya sea porque uno se concentra más sin estar pendiente de todas las herramientas de formatos o por gusto propio. En estos casos se podría utilizar Markdown.
El uso de Markdown es la forma que se transforma la documentación y lo manuales a una forma similar al código fuente de un programa para que sea gestionado de la misma forma y en el mismo lugar que el programa.
